//============================================================================
// Name        : olas.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <fstream>
#include <cstring>
#include <cstdlib>
#include <vector>
#include <sstream>
using namespace std;

struct tipoP{
	string cod;
	string descripcion;
};

struct producto{
	struct tipoP tipo;
	string codigo;
	string nombre;
	string porciones;
	float precio;
	int cantidad;
	string promocion;

};

struct ResultadoBusqueda{
	int indice;
	struct producto datosProducto;
};

vector<struct producto> listaProductos;
string nombreArchivo = "datos.txt";
const string nohaydatos="";
const int nohayNum = -1;
const int registroNoEncontrado = -2;

string convertirCadena(struct producto Producto){
	stringstream ss;
	ss<<Producto.tipo.cod <<";" << Producto.tipo.descripcion << ";"<<Producto.codigo<<";"<<Producto.nombre <<";" << Producto.porciones << ";"<< Producto.precio <<";"<< Producto.cantidad << ";" << Producto.promocion << endl;
	return ss.str();
}

struct producto leerRegistroProducto(string linea){
	stringstream tmp(linea);
	string campo1 , campo2;
	struct producto resultado;
	getline(tmp,resultado.tipo.cod,';');
	getline(tmp,resultado.tipo.descripcion,';');
	getline(tmp,resultado.codigo,';');
	getline(tmp,resultado.nombre,';');
	getline(tmp,resultado.porciones,';');
	getline(tmp,campo1,';');
	getline(tmp,campo2,';');
	getline(tmp,resultado.promocion,';');
	resultado.precio = atof(campo1.c_str());
	resultado.cantidad = atoi(campo2.c_str());
	return resultado;
}

void cargarDatos(){
	ifstream archivo(nombreArchivo.c_str());
	string linea;
	if(archivo.is_open()){
		struct producto p;
		while(getline(archivo,linea)){
			p = leerRegistroProducto(linea);
			listaProductos.push_back(p);
		}
	}else{
		cout<<"Error al abrir archivo";
		return;
	}
	archivo.close();
}

void mostrarProducto(struct producto Producto){
	cout<<"Codigo del tipo de producto: "<<Producto.tipo.cod<<endl;
	cout<<"Nombre del tipo de producto: "<<Producto.tipo.descripcion<<endl;
	cout<<"Codigo del producto: "<<Producto.codigo<<endl;
	cout<<"Nombre: "<<Producto.nombre<<endl;
	cout<<"Cantidad de porciones: "<<Producto.porciones<<endl;
	cout<<"Precio: "<<Producto.precio<<endl;
	cout<<"Cantidad disponible: "<<Producto.cantidad<<endl;
	cout<<"Promocion asociada: "<<Producto.promocion<<endl;
	cout << "holi"<< endl;
}

void mostrarProductos(){
	for(int i=0;i<listaProductos.size();i++){
		cout << "Producto " << i+1 << ":" << endl;
		mostrarProducto(listaProductos[i]);
		cout<<"*********************************"<<endl;
	}
}

void almacenarDatos(){
	ofstream archivo(nombreArchivo.c_str());

	if(archivo.is_open()){

		for(int i=0;i<listaProductos.size();i++){
			archivo<<convertirCadena(listaProductos[i]);
		}
	}else{
		cout<<"Error al abrir archivo";
		return;
	}
	cout<<"Productos registrados correctamente... "<<endl;
	archivo.close();
}

void agregarProducto(struct producto Producto){
	listaProductos.push_back(Producto);
}



struct ResultadoBusqueda buscarProducto(string codigo){
	struct ResultadoBusqueda resultado;
	struct producto registro;
	for(int i=0;i<listaProductos.size();i++){
		if(listaProductos[i].codigo==codigo){
			cout<<"Producto encontrado ... "<<endl;
			mostrarProducto(listaProductos[i]);
			resultado.datosProducto =listaProductos[i];
			resultado.indice = i;
			return resultado;
		}
	}
	cout<<"Producto no encontrado ..."<<endl;
	return resultado;
}

void actualizarProducto(int indice, struct producto datos){
	struct producto registro = listaProductos[indice];

	if(datos.precio!=nohayNum){
		registro.precio = datos.precio;
	}
	if(datos.cantidad!=nohayNum){
		registro.cantidad = datos.cantidad;
	}
	if(datos.promocion!=nohaydatos){
		registro.promocion = datos.promocion;
	}

	listaProductos[indice] = registro;
}
void eliminarProducto(int indice){
	listaProductos.erase(listaProductos.begin()+indice);
	cout<<"Producto eliminado correctamente"<<endl;
}

void Buscar(){
	string codigo;
	cout<<"Ingrese codigo: ";
	cin>>codigo;

	buscarProducto(codigo);

}

void Actualizar(){
	string codigo;
	string campo;
	float n;
	cout<<endl<<"ACTUALIZACION DE PRODUCTO"<<endl<<"***************************"<<endl;
	cout<<"Ingrese el codigo del producto que desea actualizar: ";
	cin>>codigo;
	ResultadoBusqueda resultado = buscarProducto(codigo);
	int indice = resultado.indice;
	struct producto Product =resultado.datosProducto;

	if(indice!=registroNoEncontrado){
		cin.ignore();

		cout<<"Ingrese nuevo valor de precio o presione ENTER para mantener el anterior: ";
		getline(cin,campo);
		if(campo!=nohaydatos){
			Product.precio = atof(campo.c_str());
		}

		cout<<"Ingrese nueva cantidad o presione ENTER para mantener el anterior: ";
		getline(cin,campo);
		if(campo!=nohaydatos){
			Product.cantidad = atof(campo.c_str());
		}
		cout<<"Ingrese la  nueva promoción asociada o presione ENTER para mantener el anterior: ";
		getline(cin,campo);
		if(campo!=nohaydatos){
			Product.promocion = campo;
		}
		actualizarProducto(indice,Product);
	}
}

void Registrar(){

	struct producto p;

	cout<<"Ingrese codigo: ";
	cin>>p.codigo;
	cin.ignore();
	cout<<"Ingrese nombre: ";
	//cin>>p.nombre;
	getline(cin,p.nombre);
	cout<<"Ingrese codigo del tipo de masa para esta torta: ";
	//getline(cin,p.tipo.cod);
	cin>>p.tipo.cod;
	cin.ignore();
	cout<<"Ingrese nombre del tipo de masa para esta torta: ";
	getline(cin,p.tipo.descripcion);
	//cin>>p.tipo.descripcion;
	//cin.ignore();
	cout<<"Ingrese cantidad de porciones para esta torta: ";
	getline(cin,p.porciones);
	//cin>>p.porciones;
	cout<<"Ingrese precio: ";
	cin>>p.precio;
	cout<<"Ingrese cantidad del producto: ";
	cin>>p.cantidad;
	cin.ignore();
	cout<<"Ingrese promocion asociada: ";
	getline(cin,p.promocion);
	//cin>>p.promocion;
	cout << "Listo" << endl;

	agregarProducto(p);

}


void Eliminar(){
	string codigo;
	char resp;
	cout<<endl<<"ELIMINACION DE PRODUCTO"<<endl<<"***************************"<<endl;
	cout<<"Codigo de producto a eliminar: ";
	cin>>codigo;
	int indice = buscarProducto(codigo).indice;
	if(indice!=registroNoEncontrado){
		cout<<"Confirme eliminacion del producto mostrado (S/N): "<<endl;
		cin>>resp;
		if(resp=='S' || resp=='s'){
			eliminarProducto(indice);
		}else{
			cout<<"Accion cancelada... "<<endl;
		}
	}else{
		cout<<"Registro no fue eliminado... "<<endl;
	}

}

int Menu() {

	int opcion;
	while(true){

		cout<<endl<<"TORTAS"<<endl<<"**********"<<endl;
		cout<<"Seleccione una opcion:"<<endl;
		cout<<"<1> Registrar Producto"<<endl;
		cout<<"<2> Buscar Producto"<<endl;
		cout<<"<3> Actualizar Producto"<<endl;
		cout<<"<4> Eliminar Producto"<<endl;
		cout<<"<5> Mostrar todos los productos"<<endl;
		cout<<"<6> Salir"<<endl;

		cin>>opcion;

		switch(opcion){
			case 1:
				Registrar();
				//cout << "HOLA" << endl;
				break;
			case 2:
				Buscar();
				break;
			case 3:
				Actualizar();
				break;
			case 4:
				Eliminar();
				break;
			case 5:
				mostrarProductos();
				break;
			case 6:
				return 0;
				break;
			default:
				cout<<"No es una opcion valida"<<endl;
		}
		cin.ignore();
		cout<<"Presione una tecla para retornar al menu..."<<endl;
		cin.get();
		system("cls");

	}
	return 1;
}

int main() {
	cargarDatos();
	Menu();
	almacenarDatos();
	return 0;
}
