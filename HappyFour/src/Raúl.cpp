//============================================================================
// Name        : trabajo.cpp
// Author      : Ra�l
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <cstdlib>

#include <queue>
#include <cstdlib>

#include<windows.h>
#include<string.h>
using namespace std;

struct tipoP{
	string cod;
	string descripcion;
};
struct producto{
	struct tipoP tipo;
	string codigo;
	string nombre;
	string porcion;
	float precio;
	int cantidad;
	string promocion;

};

struct cliente{
	string apellidos;
	string nombres;
	string telefono;
	string correo;
};

struct boleta{
	struct cliente cliente;
	string codigo;
	string producto;
	float precio;
	int cantidad;
	float pTotal;
};

struct boleta generarBoleta(struct producto p1, int c){
	struct cliente c1;
	struct boleta b1;
	//struct producto p1;
	b1.cliente=c1;
	b1.codigo=p1.codigo;
	b1.producto=p1.nombre;
	b1.precio=p1.precio;
	b1.cantidad=c;
	b1.pTotal=p1.precio*c;
	return b1;
}

struct producto productoNuevo(struct producto pn, int c){
	struct producto p;
	p.tipo.cod=pn.tipo.cod;
	p.tipo.descripcion=pn.tipo.descripcion;
	p.codigo=pn.codigo;
    p.nombre=pn.nombre;
    p.porcion=pn.porcion;
    p.precio=pn.precio;
	p.cantidad=pn.cantidad-c;
	p.promocion=pn.promocion;
	return p;
}

void mostrarCliente(struct cliente c1){
	cout<<"Cliente: "<<c1.nombres<<" "<<c1.apellidos<<endl;
	cout<<"-----------------------------------------"<<endl;
}

void mostrarBoleta(struct boleta b1){
	cout<<"COD: "<<b1.codigo<<endl;
	cout<<"PRODUCTO: "<<b1.producto<<endl;
	cout<<"PRECIO: "<<b1.precio<<endl;
	cout<<"CANTIDAD: "<<b1.cantidad<<endl;
	cout<<"SUB TOTAL: "<<b1.pTotal<<endl;
	cout<<"-----------------------------------------"<<endl;
}

vector<struct producto> listaProductos;
vector<struct producto> nuevosProductos;
vector<struct boleta> boletaVentas;

string nombreArchivo = "datos.txt";

string convertirCadena(struct producto p1){
	stringstream ss;
	ss<<p1.tipo.cod <<p1.tipo.descripcion<<p1.codigo<<";"<<p1.nombre<<";"<<p1.precio<<";"<<p1.porcion<<";"<<p1.cantidad<< ";" <<p1.promocion << endl;
	return ss.str();
}


struct producto leerRegistroProducto(string linea){
	stringstream tmp(linea);
	string campo1, campo2;
	struct producto resultado;
	getline(tmp,resultado.tipo.cod,';');
	getline(tmp,resultado.tipo.descripcion,';');
	getline(tmp,resultado.codigo,';');
	getline(tmp,resultado.nombre,';');
	getline(tmp,resultado.porcion,';');
	getline(tmp,campo1,';');
	getline(tmp,campo2,';');
	getline(tmp,resultado.promocion,';');
	resultado.precio = atof(campo1.c_str());
	resultado.cantidad = atoi(campo2.c_str());
	return resultado;
}

void cargarDatos(){
	ifstream archivo(nombreArchivo.c_str());
	string linea;
	if(archivo.is_open()){
		struct producto p;
		while(getline(archivo,linea)){
			p=leerRegistroProducto(linea);
			listaProductos.push_back(p);
		}
	}else{
		cout<<"Error al abrir archivo";
		return;
	}
	archivo.close();
}

void mostrarProducto(struct producto Producto){
	cout<<"Codigo: "<<Producto.codigo<<endl;
	cout<<"Nombre: "<<Producto.nombre<<endl;
	cout<<"Porciones: "<<Producto.porcion<<endl;
	cout<<"Precio: "<<Producto.precio<<endl;
	cout<<"Cantidad disponible: "<<Producto.cantidad<<endl;
	cout<<"Promocion asociada: "<<Producto.promocion<<endl;
}

void mostrarProductos(){
	cargarDatos();
	cout<<"*******************************************************";
	cout<<"\n\tPRODUCTOS DISPONIBLES"<<endl;
	cout<<"\nSeleccione el producto y la cantidad que desea adquirir..."<<endl;
	cout<<endl;
	for(int i=0;i<listaProductos.size();i++){
		cout<<"PRODUCTO "<<"<"<<i+1<<">"<<endl;
		mostrarProducto(listaProductos[i]);
		cout<<"***************************"<<endl;
	}
	cout<<"\nSeleccione el producto y la cantidad que desea adquirir..."<<endl;
}

/*void casos(int op, bool &salir){
	struct boleta b0;
	switch(op){
			case 0: salir=true;
			    break;
			case 1: cout<<"Cantidad: ";
			        cin>>b0.cantidad;
			        break;
			case 2: cout<<"Cantidad: ";
			        cin>>b0.cantidad;
			    	break;
		    case 3: cout<<"Cantidad: ";
		            cin>>b0.cantidad;
			    	break;
		    case 4: cout<<"Cantidad: ";
                    cin>>b0.cantidad;
	    	        break;
			case 5: cout<<"Cantidad: ";
			        cin>>b0.cantidad;
			        break;
			case 6: cout<<"Cantidad: ";
			        cin>>b0.cantidad;
			    	break;
		    case 7: cout<<"Cantidad: ";
		            cin>>b0.cantidad;
			    	break;
		    case 8: cout<<"Cantidad: ";
                    cin>>b0.cantidad;
	    	        break;
			case 9: cout<<"Cantidad: ";
			        cin>>b0.cantidad;
			        break;
			case 10: cout<<"Cantidad: ";
			        cin>>b0.cantidad;
			    	break;
		    case 11: cout<<"Cantidad: ";
		            cin>>b0.cantidad;
			    	break;
		    case 12: cout<<"Cantidad: ";
                    cin>>b0.cantidad;
	    	        break;
			case 13: cout<<"Cantidad: ";
			        cin>>b0.cantidad;
			        break;
			case 14: cout<<"Cantidad: ";
			        cin>>b0.cantidad;
			    	break;
		    case 15: cout<<"Cantidad: ";
		            cin>>b0.cantidad;
			    	break;
		    case 16: cout<<"Cantidad: ";
                    cin>>b0.cantidad;
	    	        break;
			case 17: cout<<"Cantidad: ";
			        cin>>b0.cantidad;
			        break;
			case 18: cout<<"Cantidad: ";
			        cin>>b0.cantidad;
			    	break;
		    case 19: cout<<"Cantidad: ";
		            cin>>b0.cantidad;
			    	break;
		    case 20: cout<<"Cantidad: ";
                    cin>>b0.cantidad;
	    	        break;
			case 21: cout<<"Cantidad: ";
			        cin>>b0.cantidad;
			        break;
			case 22: cout<<"Cantidad: ";
			        cin>>b0.cantidad;
			    	break;
		    case 23: cout<<"Cantidad: ";
		            cin>>b0.cantidad;
			    	break;
		    default: cout<<"\nOpci�n inv�lida...\n";
		            break;
			}
}*/


void seleccionarProducto(){
	bool salir=false;
	cout<<"Pulse 0 para terminar"<<endl;
	while(!salir){
		//system("cls");
		int op;
		cout<<"\nPRODUCTO: ";
		cin>>op;
		struct producto pd, px;
		struct boleta b0;
		switch(op){
					case 0: salir=true;
					    break;
					case 1: cout<<"Cantidad: ";
					        cin>>b0.cantidad;
					        break;
					case 2: cout<<"Cantidad: ";
					        cin>>b0.cantidad;
					    	break;
				    case 3: cout<<"Cantidad: ";
				            cin>>b0.cantidad;
					    	break;
				    case 4: cout<<"Cantidad: ";
		                    cin>>b0.cantidad;
			    	        break;
					case 5: cout<<"Cantidad: ";
					        cin>>b0.cantidad;
					        break;
					case 6: cout<<"Cantidad: ";
					        cin>>b0.cantidad;
					    	break;
				    case 7: cout<<"Cantidad: ";
				            cin>>b0.cantidad;
					    	break;
				    case 8: cout<<"Cantidad: ";
		                    cin>>b0.cantidad;
			    	        break;
					case 9: cout<<"Cantidad: ";
					        cin>>b0.cantidad;
					        break;
					case 10: cout<<"Cantidad: ";
					        cin>>b0.cantidad;
					    	break;
				    case 11: cout<<"Cantidad: ";
				            cin>>b0.cantidad;
					    	break;
				    case 12: cout<<"Cantidad: ";
		                    cin>>b0.cantidad;
			    	        break;
					case 13: cout<<"Cantidad: ";
					        cin>>b0.cantidad;
					        break;
					case 14: cout<<"Cantidad: ";
					        cin>>b0.cantidad;
					    	break;
				    case 15: cout<<"Cantidad: ";
				            cin>>b0.cantidad;
					    	break;
				    case 16: cout<<"Cantidad: ";
		                    cin>>b0.cantidad;
			    	        break;
					case 17: cout<<"Cantidad: ";
					        cin>>b0.cantidad;
					        break;
					case 18: cout<<"Cantidad: ";
					        cin>>b0.cantidad;
					    	break;
				    case 19: cout<<"Cantidad: ";
				            cin>>b0.cantidad;
					    	break;
				    case 20: cout<<"Cantidad: ";
		                    cin>>b0.cantidad;
			    	        break;
					case 21: cout<<"Cantidad: ";
					        cin>>b0.cantidad;
					        break;
					case 22: cout<<"Cantidad: ";
					        cin>>b0.cantidad;
					    	break;
				    case 23: cout<<"Cantidad: ";
				            cin>>b0.cantidad;
					    	break;
				    default: cout<<"\nOpci�n inv�lida...\n";
				            break;
					}
		if(op>0 && op<24){
			pd=listaProductos[op-1];
			b0=generarBoleta(pd,b0.cantidad);
			px=productoNuevo(pd,b0.cantidad);
			boletaVentas.push_back(b0);
			nuevosProductos.push_back(px);
		}
	}
	cout<<"---------------------------"<<endl;
	cout<<"---------------------------"<<endl;
    cout<<"Compra finalizada"<<endl;
    cout<<"\n\t#BOLETA DE VENTAS#"<<endl;
    struct cliente c1;
    float total=0;
    c1={"MORENO","JUAN","55555555","jchipi@gmail.com"};
    mostrarCliente(c1);
    for(int i=0;i<boletaVentas.size();i++){
    	mostrarBoleta(boletaVentas[i]);
    	total+=boletaVentas[i].pTotal;
    }
    cout<<"TOTAL: "<<total<<endl;
    cout<<"\nGracias por su compra :)"<<endl;

    cout<<"..................................................."<<endl;
    cout<<"\tProductos actualizados\n"<<endl;
    for(int i=0;i<nuevosProductos.size();i++){
    		cout<<"PRODUCTO "<<"<"<<i+1<<">"<<endl;
    		mostrarProducto(nuevosProductos[i]);
    		cout<<"******************************"<<endl;
    	}
	}


void menuVentas(){ //men� de ventas
cout<<"     ----------------------------------------------------------------------\n";
cout<<"      <<<<<<<<<<       TORTAS -MI SABOR, TU PERDICI�N-        >>>>>>>>>>";
cout<<"\n     ----------------------------------------------------------------------\n\n";
cout<<"      (1)MOSTRAR PRODUCTOS"<<endl;
cout<<"      (2)REGRESAR"<<endl;
cout<<"      (3)SALIR"<<endl;

bool salir=false;
while(!salir){
	int op;
	cin>>op;
	switch(op){
	case 1: mostrarProductos();
	        seleccionarProducto();
	        salir=true;
	        break;
	case 2: cout<<"<-----"<<endl;//ATRAS...
	        break;
	case 3: salir=true;
	        break;
	default: cout<<"\nOpci�n inv�lida...\n";
	        break;
	}
	cin.get();
}
}

int main(){
	menuVentas();

	return 0;
}
